package com.salt.secconf.models;

import org.springframework.beans.factory.annotation.Value;

public class Dashboard {
    @Value("${random.int(1000,9999)}")
    private String identifier;
    private String name;
    private String employer;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }
}
