package com.salt.secconf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecConfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecConfApplication.class, args);
	}

}
