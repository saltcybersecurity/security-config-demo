package com.salt.secconf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class Echo {

    @GetMapping("/echo")
    public String echo(@RequestParam String victimCookie, Model model) {
        String cookieMsg;
        if (!victimCookie.isBlank()) {
            cookieMsg = victimCookie ;
        } else {
            cookieMsg = "There's no victimCookie value to display.";
        }

        model.addAttribute("victimCookie", cookieMsg);
        System.out.println(cookieMsg);
        return "cookie";
    }
}
