package com.salt.secconf.controllers;

import com.salt.secconf.models.Credential;
import com.salt.secconf.models.Dashboard;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class Login {

    @GetMapping({"/"})
    public String getIndex() {
        return "login";
    }

    @GetMapping({"/login"})
    public String getLogin(Credential form, HttpServletResponse response) {
        Cookie cookie = new Cookie("jhgsd_sess", "b700205e-2681-11ec-bef7-4d6262417853");
        Cookie cookie1 = new Cookie("salt_xjfkd8743_id", "%2247f78603-7035-4d9a-b5d1-755a5b34c758%22");
        Cookie cookie2 = new Cookie("salt_group_id", "oeu1626948159447r0.6950354954371512");
        Cookie cookie3 = new Cookie("slt_xid.current", "V2UgbG92ZSB0aGUgSHR0cE9ubHkgZmxhZyEgSGFja2VycyBkb250J3QuCg==");
        Cookie cookie4 = new Cookie("prdST.session.token", "43488337E98CAC82056C68E855787E8E");
        response.addCookie(cookie);
        response.addCookie(cookie1);
        response.addCookie(cookie2);
        response.addCookie(cookie3);
        response.addCookie(cookie4);
        return "login";
    }

    @PostMapping("/login")
    public String login(BindingResult result) {

        if (result.hasErrors()) {
            return "login";
        }
        return "update";
    }

    @GetMapping({"/update"})
    public String getUpdate(Dashboard dashboard) {
        return "update";
    }


}
