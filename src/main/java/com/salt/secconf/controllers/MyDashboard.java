package com.salt.secconf.controllers;

import com.salt.secconf.models.Dashboard;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;

@Controller
@RolesAllowed("BASIC_USER")
public class MyDashboard {
    Logger logger = LogManager.getLogger(MyDashboard.class);
    @RequestMapping("/dashboard")
    public String searchResults(@ModelAttribute Dashboard dashboard, Model model) {
        model.addAttribute("dashboard", dashboard);
        return "dashboard";
    }
    @RequestMapping("/dashboard/")
    public String searchResults(@RequestParam String id, Model model) {
        logger.info("id: {}", id);
        Dashboard dashboard=new Dashboard();
        dashboard.setIdentifier(id);
        model.addAttribute("dashboard", dashboard);
        return "dashboard";
    }    
}
