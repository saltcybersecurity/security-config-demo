package com.salt.secconf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IFrame {

    @GetMapping("/iframe")
    public String iframe(@RequestParam String i, Model model){
    model.addAttribute("iframe",i);
        return "iframe";
    }
}
