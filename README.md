# Security Configuration Demo Application


## Inhoudsopgave
1. [Security Configuration Demo Application](#markdown-header-security-configuration-demo-application)
2. [Voorbereiding](#markdown-header-voorbereiding)
3. [Installatie](#markdown-header-installatie)
4. [Tools](#markdown-header-tools)
5. [Opdrachten](#markdown-header-opdrachten)
    1. [HTTP Only flag uitbuiten](#markdown-header-1-http-only-flag-uitbuiten)
    2. [Evil Iframes](#markdown-header-2-evil-iframes)
    3. [Cross-site-scripting (XSS)](#markdown-header-3-cross-site-scripting)
    4. [SSL Certificates](#markdown-header-4-ssl-certificates)
    5. [Fingerprinting webserver](#markdown-header-5-fingerprinting-webserver)
    6. [Kwetsbare afhankelijkheden](#markdown-header-6-kwetsbare-afhankelijkheden)


## Salt Customer Dashboard
Welkom op het Salt Customer Dashboard (SCD). 
SCD is een applicatie die gebrukt wordt om kwetsbare security configuratie te demonstreren. De applicatie is gebouwd met Spring boot en Thymeleaf als presentatie laag. SCD maakt gebruikt van Docker en Docker Compose om de omgeving op te zetten.

Bij het opstarten worden er 3 docker container opgestart. Een container voor het draaien van een apache webserver en twee containers die allebij dezelfde java applicatie opstarten. 

![Docker Setup](/readme-images/docker.png)

## Voorbereiding
Voor deze hands-on lab oefening is een docker omgeving met docker-compose nodig.  
Kies één van de volgende opties om dit voor te bereiden. Of sla deze stap over als je al over docker en docker-compose beschikt.  

**Docker binnen een nieuwe Virtual Machine met Ubuntu Server** 

* Volg de stappen zoals beschreven in: [ubuntu-server-instructies.md](/ubuntu-server-instructies.md)    

**Docker binnen een eigen omgeving** 

* Installeer Docker: [Docker download](https://docs.docker.com/get-docker/)
* Installeer Docker Compose: [Docker compose install](https://docs.docker.com/compose/install/)
  
## Installatie
1. Voer het volgende commando uit: `git clone https://bitbucket.org/saltcybersecurity/security-config-demo.git`.
2. Open de folder via het commando `cd security-config-demo`.
3. Open de infra folder. `cd infra`.
4. Start Docker.
5. Installeer en run de applicatie. Dit kan op de voorgrond `docker-compose up --build` of achtergrond `docker-compose up --build -d`.
6. Pas de Host file aan op de machine waar je zometeen de browser gaat gebruiken.
    1. Voor MAC OS / Linux: 
        1. `sudo nano /etc/hosts`
        2. Indien lokaal (Linux): Voeg de volgende regel toe: `172.18.0.5 evildomain.com saltcustomerdashboard.com`.
        3. Indien lokaal (MAC OS): Voeg de volgende regel toe: `127.0.0.1 evildomain.com saltcustomerdashboard.com`.
        4. LET OP: kijk uit voor dubbele ip notities in de host file.
        5. Indien via virtual machine: `[ip-adress-van-vm] evildomain.com saltcustomerdashboard.com`.
        6. Sla het bestand op.
    2. Voor Windows: 
        1. Druk op de Windows toets.
        2. Type Notepad in het zoekveld.
        3. In de zoekresultaten, rechtermuisklik op Notepad en selecteer Run as administrator.
        4. Binnen Notepad, open het volgende bestand: c:\Windows\System32\Drivers\etc\hosts.
        5. Indien lokaal: Voeg de volgende regel toe: `127.0.0.1 evildomain.com saltcustomerdashboard.com`.  
        6. Indien via virtual machine: `[ip-adress-van-vm] evildomain.com saltcustomerdashboard.com`.  
        7. LET OP: kijk uit voor dubbele ip notities in de host file.    
        8. Sla het bestand op.  
7. Bekijk de applicatie [evildomain.com](http://evildomain.com) en [saltcustomerdashboard.com](http://saltcustomerdashboard.com).

> Tijdens de opdrachten is het nodig om docker opnieuw te laten builden. Hiervoor kan je beter eerst de docker service down brengen. Dit kan met het volgende commando: `docker-compose down`

Voorbeeld van de Security Demo Applicatie na installatie:

![Security Demo Login](/readme-images/login.png)

## Tools
1. Code editor ([VS Code](https://code.visualstudio.com/))
2. Openssl (Extra voor als je zelf een SelfSigned Certificaat wilt aanmaken.)
3. Netcat of Telnet
4. Recente browser ([EDGE](https://www.microsoft.com/en-us/edge?r=1) / [Chrome](https://www.google.com/chrome/) / [Firefox](https://www.mozilla.org/en-US/firefox/new/))
5. Een FTP client die SFTP ondersteunt: [Filezilla](https://filezilla-project.org/download.php?type=client)
6. Postman


## Opdrachten
Start de applicatie op [saltcustomerdashboard.com](http://saltcustomerdashboard.com). De evildomain.com zal gebruikt worden om verschillende exploits te laten zien. Deze pagina hoef je voor nu niet te openen.

Deze workshop bestaat uit 6 opdrachten die een beeld geven van hoe een applicatie minimaal beveilgd zou moeten zijn op configuratie niveau. Het is belangrijk dat de opdrachten in de aangeven volgorge voltooid worden, omdat ze van elkaar afhankelijk kunnen zijn.


### 1. HTTP Only flag uitbuiten
Standaard worden niet alle cookies beveilgd met een `HttpOnly` vlag. Dit kan ervoor zorgen dat een cookie uitgelezen wordt door een gevaarlijk script en zo bijvoorbeeld jouw session id gebruikt kan worden door een aanvaller.

#### 1a. Vind alle sessie cookies in saltcustomerdashboard.com
Om in te loggen in de applicatie, pak de gebruikersnaam `salt` met wachtwoord `123`.

Via een cross-site-scripting (XSS) aanval zou een aanvaller de cookiess van een gebruiker kunnen uitlezen. Wanneer een aanvaller in staat is om de sessiecookie te bemachtigen, is het mogelijk om deze sessie van de gebruiker over te nemen.

Probeer te achterhalen welke sessie cookies er zijn (non-persistent cookies) die de applicatie heeft gegenereerd.  

* Welke cookies zijn er te zien?  
* Wat valt je op aan de cookies?  

#### 1b. Javascript code en cookies lezen
Probeer door middel van een reflected XSS aanval de cookie waardes uit te lezen en weer te geven voor de ingelogde gebruiker `salt`. 

* Hoe ziet jouw Javascript code eruit?  

#### 1c. Javascript code en cookies lezen via een phishing link
Het voorbeeld van 1b is niet heel aantrekkelijk voor een hacker. Hij of zij moet het slachtoffer verleiden tot het plaatsen van een malafide script. Dergelijke attacks zie je ook in andere verschijningen, zoals de ***self-xss***. Daarbij wordt het slachtoffer verleid het script rechtstreeks in de browser console te plaatsen.  

Een phishing link is een stuk gevaarlijker. De opdracht is om een phishinglink te constureren. De hacker heeft al een endpoint klaarstaan op evildomain.com: `http://evildomain.com/echo?victimCookie=[de_cookie_waardes]`.
Probeer een link te construeren dat begint met `http://saltcustomerdashboard.com/dashboard/?id=` . de `id` queryparameter moet vervolgens een (URL encoded) xss snippet  bevatten.

Het resultaat moet zijn dat in evildomain.com alle cookie values geprint worden. Deze cookie values zijn dan in het bezit van de hacker.

#### 1d. Vind het echte session cookie van Spring Security
De cookies bij stap 1c zijn niet allemaal in gebruik. De ontwikkelaars van de app wilden de hackers het moeilijker maken door het standaard JSESSIONID cookie van naam te veranderen, en daarnaast een aantal fake cookies te genereren.  

* Probeer te achterhalen welk cookie de oorspronkelijke JSESSIONID was.
* Hoe wordt deze manier van beveiligen ook wel genoemd?
* Doe een poging om met Postman of met curl de sessie over te nemen van het slachtoffer: `curl -v --cookie "cookienaam=waarde" `http://saltcustomerdashboard.com/dashboard`)
* Valideer dat de HTML inderdaad het Dashboard teruggeeft. Dit is een protected page, en dus het bewijs dat de attack succesvol is.

#### 1e. Bescherm de sessie cookie
Om ervoor te zorgen dat de cookies niet meer kunnen worden gewijzigd aan de frontend gaan we de HttpOnly flag activeren. Dit kan gedaan worden op 2 verschillende manieren:

1. Via de webserver, op deze manier kunnen we alle cookies die de webserver passeren een `HttpOnly` vlag meegeven. Dit kan m.b.v. de `Header` directive.
2. Via de applicatie, op deze manier kunnen we per cookie aangeven of deze `HttpOnly` moet bevatten. Dit kan in een configuratiebestand van de applicatie.

Probeer voor beide manieren een oplossing te vinden.

> Wat heeft nu de voorkeur? Je zou optie 2 kunnen zeggen. De applicatie levert dan van zichzelf al veilige cookies. Echter, je ziet dat een web server vaak sneller te configureren is en gelijk voor meerdere apps of services tegelijk. De combinatie maakt het geheel nóg sterker.

### 2. Evil Iframes
Een iframe is een afkorting voor Inline Frame en is een techniek om de informatie van een andere webpagina binnen dezelfde (huidige) pagina weer te geven, door middel van een speciaal daarvoor bedoelde HTML tag. Hoewel het veelgebruikt wordt, brengt het toepassen van een iframe security risico's met zich mee.


#### 2a. Onderzoek een Iframe kwetsbaarheid
Klik op de volgende link [saltcustomerdashboard.com](http://evildomain.com/iframe?i=http://saltcustomerdashboard.com/dashboard). Hierbij kun je zien dat het net lijkt of de website saltcustomerdashboard.com op een legitieme manier geladen wordt en verwacht je wellicht dat je ook op saltcustomerdashboard.com terecht komt.

Is dit wel zo? Een aanvaller heeft in dit geval een iframe geplaatst op zijn/haar website om een gebruiker te misleiden. 
* Wat is er nu veranderd aan de opmaak van de pagina t.o.v. dezelfde pagina op saltcustomerdashboard.com?
* Wat voor mogelijkheden zou een attacker hiermee hebben?
 
Clickjacking, ook bekend als een `UI redress attack`, is een aanval waarbij een aanvaller een gebruiker te misleid om op een knop of link op een andere pagina te klikken. De aanvaller 'kaapt' dus klikken die bedoeld zijn voor hun pagina en leidt ze naar een andere pagina.

#### 2b. Bescherming tegen clickjacking via Iframes  
Configureer Apache zodanig, dat het niet meer mogelijk is om saltcustomerdashboard.com in een iframe ingeladen kan worden door evildomain.com.

### 3. Cross-site-scripting (XSS)
Cross-Site Scripting (XSS) is een kwetsbaarheid waarbij kwaadaardige scripts worden geïnjecteerd in in webapplicaties. Door middel van XSS kan een aanvaller een webapplicatie misbruiken om kwaadaardige code, meestal in de vorm van een client-sidescript, naar een andere eindgebruiker te sturen. XSS kwetsbaarheden treden overal op waar een webapplicatie de invoer van een gebruiker gebruikt zonder deze te valideren of te coderen.

#### 3a. XSS en de HttpOnly bescherming
In opdracht [opdracht 1](#markdown-header-1-http-only-flag-uitbuiten) heb je een script geschreven die de cookies door kon sturen naar evildomain.com. Echter, als je de opdracht goed hebt gedaan, is in ieder geval het session-id cookie beschermd. 
Controleer één van de twee punten (afhankelijk van welke bescherming je hebt gekozen voor HttpOnly):

* Valideer dat er geen enkel cookie meer doorgestuurd wordt naar evildomain.com.
* Valideer dat alleen het betreffende gemaskeerde session-id cookie niet doorgegeven wordt.

Als het goed is kom jij ook tot de conclusie dat door de HttpOnly-vlag aan te passen, de cookies nu niet meer leesbaar via Javascript. Het is echter nog steeds mogelijk om een xss attack uit te voeren. De cookies zijn dan niet meer te lezen, maar er zijn nog steeds mogelijkheden voor attackers. Denk aan het kunnen beïnvloeden van bepaalde bestaande javascripts, het aanpassen van (onclick) events en hyperlinks te wijzigen.

#### 3b. Pas de phishing link aan
De phishing link is niet meer effectief voor het lezen van cookies. Pas het voor nu even aan dat er een ***alert*** zichtbaar wordt (als bewijs van een XSS attack).


#### 3c. Configureer een Content Security Policy
Content-Security-Policy is de naam van een HTTP-response header die browsers gebruiken om de beveiliging van een webpagina te verbeteren. Met deze header kun je afdwingen hoe de browser bronnen zoals Javascript, CSS, afbeeldingen, etc. valideert en toestaat. Dit helpt om aanvallen zoals XSS, te voorkomen. De term Content Security Policy wordt vaak afgekort als CSP.

De makkelijkste manier om een CSP header te plaatsen, is in de configuratie van Apache. Een goede configuratie zorgt ervoor dat Javascript dat niet afkomstig is van saltcustomerdashboard.com, niet zomaar kan worden uitgevoerd.
* Definieer en configureer een CSP header in de Apache configuratie die er voor zorgt dat de XSS attack bij 3b niet meer werkt.

#### 3d. Broncode aanpassing 
Naast het congifureren van een CSP header in Apache om XSS te voorkomen, kan ook in de broncode een aanpassing worden gedaan om XSS te bemoelijken. Probeer in de code een aanpassing te doen die dit kan realiseren. Tip: kijk in de Thymeleaf templates in de applicatie en onderzoek welke HTML tags je kunt gebruiken hiervoor.


### 4. SSL Certificates
Wanneer informatie tussen de client en de server wordt verzonden, moet deze worden versleuteld en beveiligd om te voorkomen dat een aanvaller deze kan lezen of wijzigen. Dit wordt meestal gedaan met behulp van HTTPS, dat gebruikmaakt van het Transport Layer Security (TLS) -protocol, een vervanging voor het oudere Secure Socket Layer (SSL) -protocol. TLS biedt de server ook een manier om aan de client aan te tonen dat ze verbinding hebben gemaakt met de juiste server, door een vertrouwd digitaal certificaat te presenteren.

In de loop der jaren zijn er een groot aantal cryptografische zwakheden geïdentificeerd in de SSL- en TLS-protocollen, evenals in de cijfers die ze gebruiken. Bovendien hebben veel van de implementaties van deze protocollen ook ernstige kwetsbaarheden. Daarom is het belangrijk om te weten hoe je TLS op een veilige implementeerd.

#### 4a. Configureer SSL verbinding
Voor deze opdracht is het de bedoeling dat de het verkeer van saltcustomerdashboard.com via een SSL verbinding gaat verlopen. Om dit voor elkaar te krijgen zal er eerst een certificaat gemaakt moeten worden en zal deze op port 443 geactiveerd moeten worden. Dit kan met behulp van de tool opensource tool `openssl`.***

> Mocht het niet lukken om openssl te installeren op je machine dan kan je ook gebruik maken van het certifcaat wat je terug kan vinden in de infra folder. Het is hierbij de bedoeling dat de `apache-selfsigned.crt` en `apache-selfsigned.key` in de apache webserver geplaatst worden. De `Dockerfile` kan hierbij gebruikt worden.  

#### 4b. Stel het SSL poort
Stel in dat de juiste poort open staat voor een secure verbinding. Dit kan je aanpassen in de `docker-compose.yml` file.

#### 4c. Activeer SSL
Als laatste stap moet de `httpd-ssl.conf` geactiveerd worden.  

Om te testen of we een secure verbinding hebben opgezet kan je het beste FireFox als browser gebruiken. Deze laat het nog toe om op een selfsigned certificaat verder te gaan.

Nu SSL aanstaat kunnen we ook een extra beveiligings slag uitvoeren voor de cookies (die we in opdracht 1 als veiliger hebben gemaakt met HttpOnly). Dit kan doormiddel van de `Secure` vlag. Doormiddel van deze vlag kan je forceren dat de cookies alleen over een HTTPS verbinding verstuurd mogen worden. 

#### 4d. Activeer secure cookies
Probeer de Set-Cookie header aan te passen zodat deze ook `Secure` gebruikt.

#### 4e. Test de TLS/SSL verbinding op zwakheden
Probeer mnet de tool `sslyze` te achterhalen of er nog meer security aanpassingen gedaan kunnen worden de webserver. Dit commando kan je hiervoor gebruiken:  
`docker run --rm -it --network="infra_salt_network" nablac0d3/sslyze:5.0.0 apache`.

#### 4f. Verbeter de TLS/SSL zwakheden
In het verslag wat door sslyze gedaan wordt kan je zien dat er voor TLS en Ciphers een kwetsbaarheid zit. Probeer deze meldingen te verhelpen. Voer het commando in `4e.` nogmaals uit om te kijken of dit gelukt is.

### 5. Fingerprinting webserver

Via webserver-fingerprinting is het mogelijk om het type en de versie van een webserver te achterhalen. Door te ontdekken op welk type webserver een applicatie draait, kunnen aanvallers gerichter te werk gaan. Zij zijn kunnen sneller bepalen of een bekende kwetsbaarheden bevat. Vooral servers met oudere versies van software zonder up-to-date beveiligingspatches kunnen vatbaar zijn voor bekende versie-specifieke exploits.

#### 5a. Achterhaal het versienummer en type webserver met fingerprinting
Wat is het type en versienummer van de geinstalleerde Apache webserver?  
Tip: gebruik een tool zoals `netcat`, `telnet` of een browser.

#### 5b. Controleer op kwetsbaarheden
Zoek uit of de geinstalleerde versie van de webserver kwetsbaarheden bevat en vind het bijhorende CVE ID.  

Nu het versienummer van de Apache webserver bekend is kunnen we  uitzoeken of de huidige versie bekende kwetsbaarheden bevat. Dit kan door een CVE te vinden. CVE is een afkorting voor Common Vulnerabilities and Exposures. Dit is een lijst van openbaar gemaakte kwetsbaarheden in applicaties. Wanneer iemand verwijst naar een CVE, wordt bedoeld dat een kwetsbaarheid in een applicatie of systeem is gevonden waaraan een CVE-ID-nummer is toegewezen.

Een patch is deel nieuwe software, vaak in de vorm van een installatie bestand, dat wordt uitgegeven door een software fabrikant. Het is niet alleen belangrijk om applicaties regelmatig te controleren op kwetsbaarheden, maar het is ook belangrijk om deze kwetsbaarheden zo snel mogelijk te verhelpen.

Dit kan door het patch goed op orde te hebben. Patchbeheer is de controle van een beheerder over updates van het besturingssysteem, platform of applicaties. Het omvat het identificeren van systeemfuncties die kunnen worden verbeterd of gerepareerd, het creëren van die verbetering of oplossing, het vrijgeven van een update en het valideren van de installatie van die updates. 

Nu we weten dat deze versie van Apache niet veilig genoeg is, is het belangrijk dat we deze gaan patchen.

> Wist je trouwens waar de naam Apache Server vandaan komt?  
- a patchy server :-)

#### 5c. Patch de webserver

Volg de volgende instructies en pas de `Dockerfile` zodanig dat de door jou gevonden kwetsbaarheid niet meer voorkomt:

* Rebuild je docker omgeving:
    * `docker-compose down`
    * `docker-compose up --build -d`
* Probeer nogmaal de versie te achterhalen doormiddel van `netcat` of `telnet`.
* Nu willen we juist dat het fingerprinting niet meer gebruikt kan worden door een aanvaller, hiervoor moeten we een aanpassing doen in de configuratie. Er zijn twee properties die hier bij kunnen helpen:
    * `ServerTokens`
    * `ServerSignature`
* Rebuild je docker omgeving nogmaals en kijk of je nog steeds de versie kan achterhalen.

### 6. Kwetsbare afhankelijkheden

Met een dependency checker kunnen de afhankelijkheden van een applicatie getoetst worden op kwetsbaarheden. In dit voorbeeld gebruiken we de Dependency Checker van OWASP:  

#### 6a. Installeer de Dependency Checker
Volg deze instructies om de Dependency Checker te draaien:

* Indien je geen Java hebt geïnstalleerd, of als je geen JAVA_HOME omgevingsvariabele hebt geconfigureerd, doe dat eerst. Op Ubuntu kan dit als volgt:
    * `sudo apt-get install openjdk-11-jdk-headless`
    * `sudo apt install default-jdk`
    * Test java:
        * `java -version`
        * `which java`
* Navigeer naar de home directory van de gebruiker: `cd ~`
* Download de ZIP file: `wget https://github.com/jeremylong/DependencyCheck/releases/download/v6.5.0/dependency-check-6.5.0-release.zip`
* Indien je geen unzip hebt: `sudo apt install unzip`
* `unzip dependency-check-6.5.0-release.zip`

Voor MAC OS kan je Java installeren via Brew.

* `brew install java`
* Symlink: `sudo ln -sfn /usr/local/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk`.


#### 6b. Draai maven lokaal op de binaries te genereren
Tot nu toe werd de applicatie gecompileerd in de maven docker container, en vanuit de dockerfile gekopieerd in het container image van de app.  
Het makkelijkst is om de dependency checker te kunnen draaien tegen lokaal gecompileerde binaries. Deze kunnen we als volgt genereren:

* Navigeer naar de `security-config-demo' folder
* Compileer de applicatie met maven en verpak het (`package`) naar een distibueerbaar formaat (.jar). Kies één van de volgende methoden:
   * Voer het volgende commando uit: `docker run -it --rm -v "$(pwd)":/usr/src/app -w /usr/src/app maven:3-openjdk-11 mvn clean package`
   * Gebruik je lokale maven installatie.
* Als alles goed is, wordt er nu een `target` folder aangemaakt met daarin de gecompileerde en distibueerbare applicatie.  

#### 6c. Voer de dependency checker uit
* Navigeer naar de home directory van de gebruiker: `cd ~`
* Bekijk de helpinformatie van de depencency checker: `./bin/dependency-check.sh -h`
* Voer de dependency checker uit: `./bin/dependency-check.sh --out . --scan [pad naar de target folder binnen security-config-demo]`

> De dependency checker kan er de eerste keer lang over doen. Dit komt omdat de NVD repositories ingeladen worden. Deze worden lokaal bewaard, waardoor de volgende keer sneller verloopt. De dependency checker controleert zelf op updates tijdens het draaien.  

#### 6d. Bekijk het gegenereerde rapport
Er wordt door de dependency checker rapport gegenereerd in HTML formaat. Bekijk deze in een browser. Als je dit binnen de Virtual Machine hebt uitgevoerd, en deze heeft geen grafische desktop zoals Ubuntu Server, kopieer dan het bestand lokaal. Dit kan bijv. met Filezilla. Gebruik dezelfde connectiedetails als je SSH verbinding.  

#### 6e. Repareer de kwetsbaarheden
Welke packages zijn volgens het rapport kwetsbaar?  

* Bonus: probeer de Log4j kwetsbaarheid uit te buiten. Tip: in de eerdere phishing link wordt de "id" parameter gelogd.
* Werk de applicatie (pom.xml) bij en zorg dat de afhankelijheden geen kwetbaarheden meer tonen van het type HIGH of CRITICAL.
* Genereer een nieuw rapport op basis van jouw wijzigingen.