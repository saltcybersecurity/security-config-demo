## Hints
### 1. HTTP Only flag uitbuiten
#### 1a. Vind alle sessie cookies in saltcustomerdashboard.com
Probeer te achterhalen of de demo applicatie gebruik maakt van `HttpOnly` cookies. Je kunt dit het beste opzoeken d.m.v. de development tools in je browser. Het opvragen van de development tools kan via: voor Windows / Linux `F12` en voor Mac OS `Cmd + Opt+ I`. Onder de tab "Application -> Cookies" kan je de cookies van deze applicatie bekijken. 

#### 1b. Javascript code en cookies lezen

* Na het inloggen, kan in het registratiescherm, in één van de invoervelden, een stuk javascript geplaatst worden.
* We kunnen `document.cookie` gebruiken voor het uitlezen van de cookies.
* Als voorbeeld `<script>alert('Hello World');</script>`

#### 1c. Javascript code en cookies lezen via een phishing link

* Gebruik `document.cookie` voor het uitlezen van de cookie waarden (zoals in opdracht 1b.) en `window.location` voor een redirect.
* `window.location=url` kan gebruikt worden om een redirect te maken naar een andere url.
* Denk aan de richting als `http://saltcustomerdashboard.com/dashboard/?id=<script>locatie + cookie</script>`

#### 1d. Vind het echte session cookie van Spring Security
Doormiddel van het verwijderen van cookies kan je achterhalen welke het sessie ID bijhoudt. Bij het verwijderen van je huidige sessie ID zal je namelijk uitgelogd worden bij een window refresh.

#### 1e. Bescherm de sessie cookie

* Maak gebruik van de Set-Cookie header, deze kan in de httpd.conf file geplaatst worden.
* De `application.properties` file kan hier meer info over geven.







### 2. Evil Iframes
#### 2a. Onderzoek een Iframe kwetsbaarheid
*Volg de instructies uit de opdracht*.

#### 2b. Bescherming tegen clickjacking via Iframes 

* Deze aanpassing kan gedaan worden in de httpd.conf file.
* Zoals bij opdracht 1 is dit ook een header. 
* `X-Frame-Options`.





### 3. Cross-site-scripting (XSS)
#### 3a. XSS en de HttpOnly bescherming
*Volg de instructies uit de opdracht*.

#### 3b. Pas de phishing link aan
In het request van 1c de value `document.cookie` aanpassen naar een `alert function`.

#### 3c. Configureer een Content Security Policy

* Deze aanpassing kan gedaan worden in de httpd.conf file.
* Zoals bij opdracht 1 is dit ook een header. 
* [Content Securtiy Policy](https://content-security-policy.com/).

#### 3d. Broncode aanpassing 
In Thymeleaf wordt deze tag gebruikt voor het uitprinten van de data `th:utext`. 






### 4. SSL Certificates
#### 4a. Configureer SSL verbinding
* openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout {Locatie voor key} -out {Locatie voor certifacte}
* /usr/local/apache2/ssl/apache-selfsigned.crt
* /usr/local/apache2/ssl/apache-selfsigned.key

#### 4b. Stel het SSL poort
Voor secure verbiningen wordt port 443 gebruikt.


#### 4c. Activeer SSL
/usr/local/apache2/conf/extra/httpd-ssl.conf

#### 4d. Activeer SSL
`Secure`

#### 4e. Test de TLS/SSL verbinding op zwakheden
*Volg de instructies uit de opdracht*.

#### 4f. Verbeter de TLS/SSL zwakheden
* SSLProtocol
* SSLCipherSuite
* SSLHonorCipherOrder




### 5. Fingerprinting webserver
#### 5a. Achterhaal het versienummer en type webserver met fingerprinting
Om te kijken of fingerprinting aangezet is in onze apache webserver kunnen we de tool `netcat` of `telnet` gebruiken. Open een terminal/command line en probeer de Apache versie te achterhalen. Maak gebruik van de onderstaande instructies..

* `nc 127.0.0.1 80` of `telnet 127.0.0.1 80`.
* Verstuurd een HTTP request d.m.v. HEAD method `HEAD / HTTP/1.1`
* Geef de Host aan `host: ...`

Bij een succesvol request zal dit er ongeveer uitkomen.

![Fingerprint](/readme-images/fingerprint.png)

#### 5b. Controleer op kwetsbaarheden
Om erachter te komen of dit zo is kan je deze versie opzoeken in de [CVE database](https://cve.mitre.org/cve/search_cve_list.html).

#### 5c. Patch de webserver
*Volg de instructies uit de opdracht*.





### 6. Kwetsbare afhankelijkheden
#### 6a. Installeer de Dependency Checker
*Volg de instructies uit de opdracht*.

#### 6b. Draai maven lokaal op de binaries te genereren
*Volg de instructies uit de opdracht*.

#### 6c. Voer de dependency checker uit
*Volg de instructies uit de opdracht*.

#### 6d. Bekijk het gegenereerde rapport
*Volg de instructies uit de opdracht*.

#### 6e. Repareer de snakeyaml kwetsbaarheid
* Bij de bonusopdracht: check [Uitleg fix Spring](https://spring.io/blog/2021/12/10/log4j2-vulnerability-and-spring-boot)

